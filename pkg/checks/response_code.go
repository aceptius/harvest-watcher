package checks

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

type ResponseStatusCodeCheck struct {
	client       *http.Client
	endpointUrl  string
	method       string
	successCodes []int
}

func NewResponseStatusCodeCheck(client *http.Client, url, method string, successCodes []int) *ResponseStatusCodeCheck {
	return &ResponseStatusCodeCheck{
		client:       client,
		endpointUrl:  url,
		method:       method,
		successCodes: successCodes,
	}
}

func (r ResponseStatusCodeCheck) Check(ctx context.Context) (*Result, error) {
	req, err := http.NewRequest(r.method, r.endpointUrl, nil)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)

	start := time.Now()
	resp, err := r.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	duration := time.Since(start).Seconds()

	ok := false
	for _, okCode := range r.successCodes {
		if resp.StatusCode == okCode {
			ok = true
			break
		}
	}
	return &Result{
		pass:    ok,
		message: fmt.Sprintf("Checking if url %s response codes is valid", r.endpointUrl),
		additional: map[string]interface{}{
			"ContentLength": resp.ContentLength,
			"Duration":      duration,
		},
	}, nil
}
