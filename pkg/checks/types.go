package checks

import (
	"context"
	"fmt"
)

type CheckScenario []WatcherCheck

type Result struct {
	pass       bool
	message    string
	additional map[string]interface{}
}

func (r Result) Message() string {
	return r.message
}

func (r Result) Pass() bool {
	return r.pass
}

func (r Result) GetResultMessage() string {
	return fmt.Sprintf("%s ... %t (%s)", r.message, r.pass, fmt.Sprint(r.additional))
}

type WatcherCheck interface {
	Check(ctx context.Context) (*Result, error)
}
