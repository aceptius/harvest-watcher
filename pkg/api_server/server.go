package api_server

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

type Configuration struct {
}

func New(router *mux.Router, conf Configuration) *Server {
	return &Server{
		router: router,
		conf:   conf,
	}
}

type Server struct {
	router *mux.Router
	conf   Configuration
}

func (s Server) Start() error {
	srv := &http.Server{
		Handler: s.router,
		Addr:    ":8333",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	fmt.Printf("Server starting on addr: %s", srv.Addr)
	fmt.Println()
	return srv.ListenAndServe()
}
