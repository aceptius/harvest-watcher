package helpers

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	keyContentType        = "Content-Type"
	headerContentTypeJson = "application/json"
)

func JsonResponse(w http.ResponseWriter, resp interface{}) {
	respBytes, err := json.Marshal(resp)
	if err != nil {
		ApplicationError(w, fmt.Errorf("error creating json response: %w", err))
		return
	}
	w.Header().Set(keyContentType, headerContentTypeJson)
	_, _ = w.Write(respBytes)
}

func ApplicationError(w http.ResponseWriter, err error) {
	//TODO: domysliet logovanie

	w.WriteHeader(http.StatusInternalServerError)
}
