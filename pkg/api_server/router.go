package api_server

import (
	"aceptius/harvest-watcher/pkg/api_server/middleware"
	"aceptius/harvest-watcher/pkg/status"
	"github.com/gorilla/mux"
)

func CreateRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/v1/status", middleware.RequestResponseLogger("test", status.Handler()))
	return r
}
