package job

import (
	"aceptius/harvest-watcher/pkg/message_brooker"
	"errors"
	"fmt"
)

const jobWorkerExchange = "suites.job"
const jobConsumerTagMask = "suite-job-worker-%s"

var WorkerConsumeError = errors.New("worker fail to consume message")

// tag - consumer tag
// routingKey - exchange routing key
type Worker struct {
	routingKey string
	broker     message_brooker.Broker
	config     message_brooker.ConsumeConfig
}

func NewWorker(tag, queue, routingKey string, broker message_brooker.Broker) *Worker {
	return &Worker{
		routingKey: routingKey,
		broker:     broker,
		config: message_brooker.ConsumeConfig{
			ConsumerTag: fmt.Sprintf(jobConsumerTagMask, tag),
			Queue:       queue,
		},
	}
}

func (w *Worker) publishResult() {

}

func (w *Worker) Init() error {
	if w.config.Queue == "" {
		return errors.New("queue can not be empty")
	}

	if err := w.broker.DeclareDirectExchangeWithQueue(jobWorkerExchange, w.config.Queue, w.routingKey); err != nil {
		return fmt.Errorf("error init worker: %w", err)
	}

	return nil
}

func (w *Worker) Do() error {
	if err := w.broker.Consume(w.config, func(msg message_brooker.Message) (bool, bool, error) {
		fmt.Println(string(msg.GetBody()))
		w.publishResult()

		return true, false, nil
	}); err != nil {
		return fmt.Errorf("%w: %s", WorkerConsumeError, err)
	}
	return nil
}
