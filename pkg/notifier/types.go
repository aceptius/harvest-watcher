package notifier

import "context"

type Message struct {
	Text string `json:"text"`
}

type Configuration map[string]interface{}

type Definition struct {
	Tag  string
	Init func(config Configuration) (Client, error)
}

type Client interface {
	Send(ctx context.Context, message Message) error
}
