package notifier

import "context"

var AvailableClients = []Definition{
	{
		Tag: "slack",
		Init: func(config Configuration) (notifier Client, e error) {
			return NewSlackClient(config)
		},
	},
}

func NewNotificator() (*Notificator, error) {
	return &Notificator{}, nil
}

type Notificator struct {
	clients []Client
}

func (n *Notificator) Notify(ctx context.Context, msg Message) error {
	for _, c := range n.clients {
		//TODO: osetrit error
		_ = c.Send(ctx, msg)
	}
	return nil
}
