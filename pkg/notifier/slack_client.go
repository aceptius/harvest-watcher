package notifier

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type SlackConfiguration struct {
	WebHookUrl string
}

func NewSlackClient(conf Configuration) (*SlackClient, error) {
	return &SlackClient{
		config: SlackConfiguration{
			WebHookUrl: "aaa",
		},
	}, nil
}

type SlackClient struct {
	config SlackConfiguration
}

func (n SlackClient) Send(ctx context.Context, message Message) error {
	return createMessageRequest(ctx, n.config, message)
}

func createMessageRequest(ctx context.Context, config SlackConfiguration, message Message) error {
	slackBody, err := json.Marshal(message)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, config.WebHookUrl, bytes.NewReader(slackBody))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req = req.WithContext(ctx)

	client := http.DefaultClient
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error in request to slack webhook: %w", err)
	}
	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(resp.Body); err != nil {
		return fmt.Errorf("error reading response from slack webhook: %w", err)
	}
	if buf.String() != "ok" {
		return errors.New("non-ok response returned from Slack")
	}
	return nil
}
