package events

import "io"

type ExternalEvent interface {
	GetBody() io.Reader
	GetTag() string
}

func NewBasicExternalEvent(tag string, body io.Reader) *BasicExternalEvent {
	return &BasicExternalEvent{
		body: body,
		tag:  tag,
	}
}

type BasicExternalEvent struct {
	body io.Reader
	tag  string
}

func (b BasicExternalEvent) GetBody() io.Reader {
	return b.body
}

func (b BasicExternalEvent) GetTag() string {
	return b.tag
}

type ExternalEventDispatcher interface {
	Fire(event ExternalEvent) error
}
