package app

type Application struct {
	service *Services
	factory *Factory
}

func NewApplication() *Application {
	return &Application{
		service: newService(),
		factory: newFactory(),
	}
}

func (a *Application) Factory() *Factory {
	return a.factory
}

func (a *Application) Services() *Services {
	return a.service
}
