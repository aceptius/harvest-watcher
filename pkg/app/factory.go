package app

import (
	"aceptius/harvest-watcher/pkg/api_server"
	"aceptius/harvest-watcher/pkg/message_brooker"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

type Factory struct {
}

func newFactory() *Factory {
	return &Factory{}
}

func (f Factory) NewRouter() (*mux.Router, error) {
	return api_server.CreateRouter(), nil
}

func (f Factory) NewRabbitMessageBroker() (message_brooker.Broker, error) {
	return message_brooker.NewRabbitBroker(message_brooker.Configuration{
		Url:   viper.GetString("work_bus_rabbit"),
		VHost: viper.GetString("work_bus_rabbit_vhost"),
	}), nil
}
