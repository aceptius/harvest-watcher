package status

import (
	"aceptius/harvest-watcher/pkg/api_server/helpers"
	"net/http"
)

func Handler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//TODO
		status := Status{Version: "0.0.1"}

		helpers.JsonResponse(w, status)
	}
}
