package message_brooker

import "errors"

type Message interface {
	GetBody() []byte
	GetRoutingKey() string
}

type ConsumeConfig struct {
	ConsumerTag string
	Queue       string
}

type InitFunc func(broker Broker) error

// return delivery ok, exit consume, error (ignored)
type ConsumeFunc func(msg Message) (bool, bool, error)

type Broker interface {
	InitFunc(f InitFunc) Broker
	Close() error
	Consume(config ConsumeConfig, consumeFunc ConsumeFunc) error
	Publish(exchange string, msg Message) error
	PublishBatch(exchange string, msg Message) error
	PublishDirect(msg Message) error
	PublishDirectBatch(msg Message) error
	// declarations
	DeclareDirectExchangeWithQueue(exchange, queue, routingKey string) error
}

var InitFunctionError = errors.New("error while calling broker init function")
