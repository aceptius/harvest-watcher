package message_brooker

import (
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"time"
)

type Configuration struct {
	Url   string
	VHost string
}

type RabbitMessage struct {
	d amqp.Delivery
}

func (r RabbitMessage) GetBody() []byte {
	return r.d.Body
}

func (r RabbitMessage) GetRoutingKey() string {
	return r.d.RoutingKey
}

type RabbitBroker struct {
	conf        Configuration
	publishConn *amqp.Connection //its recommended to use separate connections for publish, consume
	consumeConn *amqp.Connection
	initFunc    InitFunc
}

func NewRabbitBroker(conf Configuration) *RabbitBroker {
	return &RabbitBroker{conf: conf}
}

func (r RabbitBroker) InitFunc(f InitFunc) Broker {
	r.initFunc = f
	return r
}

func (r RabbitBroker) Close() error {
	// close publish connection
	if r.publishConn != nil && !r.publishConn.IsClosed() {
		return fmt.Errorf("error closing publish connection: %s", r.publishConn.Close())
	}

	// close consumer connection
	if r.consumeConn != nil && !r.consumeConn.IsClosed() {
		return fmt.Errorf("error closing cunsumer connection: %s", r.consumeConn.Close())
	}

	return nil
}

func (r RabbitBroker) getConsumeConnection() (*amqp.Connection, error) {
	return r.resolveConnection(r.consumeConn)
}

// create and get new rabbit connection
func (r RabbitBroker) resolveConnection(current *amqp.Connection) (*amqp.Connection, error) {
	// is initialized and is not closed
	if current != nil && !current.IsClosed() {
		return current, nil
	}

	//create new connection
	conn, err := amqp.DialConfig(r.conf.Url, amqp.Config{
		Heartbeat: 10 * time.Second,
		Locale:    "en_US",
		Vhost:     r.conf.VHost,
	})
	if err != nil {
		return nil, err
	}

	//call init func if is defined
	if r.initFunc != nil {
		if err := r.initFunc(r); err != nil {
			return nil, fmt.Errorf("%w: %s", InitFunctionError, err)
		}
	}
	//add new connection to current
	current = conn

	//return connection
	return current, nil
}

func (r RabbitBroker) Consume(config ConsumeConfig, consumeFunc ConsumeFunc) error {
	if config.Queue == "" {
		return errors.New("broker consume queue can not be empty")
	}

	con, err := r.getConsumeConnection()
	if err != nil {
		return fmt.Errorf("error while consuming: %w", err)
	}

	ch, err := con.Channel()
	if err != nil {
		return fmt.Errorf("error creating consume channel: %w", err)
	}

	turnOff := make(chan bool)

	messages, err := ch.Consume(config.Queue, config.ConsumerTag, false, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("error starting consumming messages: %w", err)
	}
	go func() {
		for msg := range messages {
			// i do not care about error for now
			ok, exit, _ := consumeFunc(RabbitMessage{d: msg})
			if ok {
				_ = msg.Ack(false)
			} else {
				_ = msg.Nack(false, false)
			}

			if exit {
				turnOff <- true
			}
		}
	}()
	<-turnOff
	return nil
}

func (r RabbitBroker) Publish(exchange string, msg Message) error {
	panic("implement me")
}

func (r RabbitBroker) PublishBatch(exchange string, msg Message) error {
	panic("implement me")
}

func (r RabbitBroker) PublishDirect(msg Message) error {
	panic("implement me")
}

func (r RabbitBroker) PublishDirectBatch(msg Message) error {
	panic("implement me")
}

func (r RabbitBroker) DeclareDirectExchangeWithQueue(exchange, queue, routingKey string) error {
	con, err := r.getConsumeConnection()
	if err != nil {
		return err
	}

	ch, err := con.Channel()
	if err != nil {
		return fmt.Errorf("error creating consume channel: %w", err)
	}

	if err := ch.ExchangeDeclare(exchange, "direct", true, false, false, false, nil); err != nil {
		return fmt.Errorf("error creating direct exchange: %w", err)
	}

	q, err := ch.QueueDeclare(queue, true, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("error creating direct queue: %w", err)
	}

	if err := ch.QueueBind(q.Name, routingKey, exchange, false, nil); err != nil {
		return fmt.Errorf("error creating direct queue binding: %w", err)
	}

	return nil
}
