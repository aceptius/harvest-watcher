### STAGE 1 - BUILD ###
FROM golang:1.13 as build

# CGO and orace driver dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
 git gcc musl-dev

# Copy project sources
ADD . /opt/harvest-watcher/
WORKDIR /opt/harvest-watcher

# build project from vendor sources
RUN go build -mod=vendor -o watcher main.go

### STAGE 2 - FINAL ###
FROM debian:9.7

# configure time zone in container
RUN ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime && echo "Europe/Prague" > /etc/timezone

# copy sources from build stage
COPY --from=build /opt/harvest-watcher/watcher /opt/harvest-watcher/

WORKDIR /opt/harvest-watcher/

ENTRYPOINT ["/opt/harvest-watcher/watcher"]

CMD ["start"]