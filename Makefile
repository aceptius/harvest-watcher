GO=GO111MODULE=on go

.PHONY: vendor test lint build format tidy

all: tidy vendor format lint test

build:
	$(GO) build

tidy:
	$(GO) mod tidy

vendor:
	# vendor all dependencies and copy goracle package sources see https://github.com/golang/go/issues/26366
	export GOHOME=$${GOPATH:-"$$HOME/go"}

	if [ ! -d vendor/ ]; then echo "no vendor directory"; exit 1; fi

	@echo "vendoring dependencies"
	$(GO) mod vendor

test:
	$(GO) test ./...

lint:
	golangci-lint run -D errcheck -E bodyclose,gofmt,unconvert --skip-dirs vendor

lint-full:
	golangci-lint run  -E bodyclose,gofmt,unconvert,errcheck --skip-dirs vendor

format:
	gofmt -s -l -w pkg/ cmd/ *.go