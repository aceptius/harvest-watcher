#### api
- autorizacia

#### Kontroly
- Kontrola platnosti certifikatu https://github.com/carlmjohnson/certinfo
- Kontrola dostupnosti webu
- Kontrola doby odozvy

#### Vystupy:
- statistiky response time/ navratove kody

#### Monitoring:
-  Prometheus monitoring 

### Napady
- poslat job na kontrolu do rabbita a nechat consumera nech to skontroluje a posle naspat message do fronty,
  tak by sa dalo nasimulovat rozne podmienky a polohy