package cmd

import (
	"aceptius/harvest-watcher/pkg/api_server"
	"aceptius/harvest-watcher/pkg/app"
	"github.com/spf13/cobra"
)

// serverCmd represents the api_server command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		f := app.Factory{}
		router, _ := f.NewRouter()

		server := api_server.New(router, api_server.Configuration{})
		_ = server.Start()
	},
}

func init() {
	rootCmd.AddCommand(startCmd)
}
