package cmd

import (
	"aceptius/harvest-watcher/pkg/checks"
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"net/http"
	"time"
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {

		ticker := time.NewTicker(10 * time.Second)
		defer ticker.Stop()

		done := make(chan bool)

		go func() {
			for {
				<-ticker.C
				check := checks.NewResponseStatusCodeCheck(http.DefaultClient, "https://www.executivejob.cz", http.MethodGet, []int{200})
				res, err := check.Check(context.Background())
				if err != nil {
					fmt.Println(err)
				}
				fmt.Println(res.GetResultMessage())
			}

		}()
		<-done
	},
}

func init() {
	rootCmd.AddCommand(checkCmd)
}
