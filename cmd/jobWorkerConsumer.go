package cmd

import (
	"aceptius/harvest-watcher/pkg/app"
	"aceptius/harvest-watcher/pkg/suite_orchestrator/job"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// workerConsumerCmd represents the workerConsumer command
var workerConsumerCmd = &cobra.Command{
	Use:   "job-worker-consumer",
	Short: "Start job worker consumer",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("workerConsumer called")
		a := app.NewApplication()

		broker, err := a.Factory().NewRabbitMessageBroker()
		if err != nil {
			logrus.WithError(err).Panicf("error creating broker")
		}

		//TODO domysliet fronty a kluce
		worker := job.NewWorker("local", "suite.jobs.local", "suite.jobs.local", broker)

		//init
		if err := worker.Init(); err != nil {
			logrus.WithError(err).Panic("Error while init worker")
		}

		if err := worker.Do(); err != nil {
			logrus.WithError(err).Panic("Job worker error")
		}
	},
}

func init() {
	rootCmd.AddCommand(workerConsumerCmd)
}
